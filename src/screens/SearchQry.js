import React, { Component } from 'react';
import {View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    TextInput,
    ScrollView,
    SafeAreaView
} from 'react-native'

import FoodCard from '../components/foodCard'
class SearchQry extends Component {
    state={
		queryText:this.props.route.params != undefined ? this.props.route.params.qry : "Search food",
		itemList:[]
	}
	componentDidMount(){
		if ( this.props.route.params != undefined && this.props.route.params.qry != 'Search food' )
			this.getFoodItems(this.props.route.params.qry);
	}
	getFoodItems = async (itemName) => {
		if ( this.state.queryText == 'Search food' || this.state.queryText == '' ){
			return true;
		}
		try {
			let response = await fetch(
				'https://search-prestostaging-xrrrt5jltpheeyvmkxzojrylvi.ap-south-1.es.amazonaws.com/category_items/_search?q=name:'+itemName
			);
			let foodItems = await response.json().then(items => {
				let fooditemsList = [];
				console.log(items.status);
				if ( items.status != 400 ){
					items.hits.hits.map( item => {
						// console.log(item);
						fooditemsList.push({
							id:item._id,
							score:item._score,
							shortDisc:item._source.description,
							name:item._source.name,
							isAvailable:item._source.is_available,
							price:item._source.price.base_price,
							calories:78,
							priceBeakup:item._source.price,
							img:require('../images/spaghetti.jpg')
						})
					})
					this.setState({
						itemList:fooditemsList
					})
					return items;
				}
			});
			return foodItems;
		} catch (error) {
			console.error(error);
		}
	};
	onChangeSearchHandler = (value) => {
		this.setState({
			queryText:value
		})
	}
    render() {
        let fooditems = [{
			id:'jndsajdj',
			name:"Rocket and tomato spaghetti",
			shortDisc:"Spagetti topped with fresh rocket",
			calories:78,
			price:100.00,
			img:require('../images/spaghetti.jpg')
		},{
			id:'sdsdsd',
			name:"Veg Sandwich",
			shortDisc:"Crunchy bread made with fresh rosemary ",
			calories:78,
			price:100.00,
			img:require('../images/sandwich.jpg')
		},{
			id:'jndsdsdajdj',
			name:"Prosciutto and urid dal salad",
			shortDisc:"Egg-free salad made from tomatoes",
			calories:78,
			price:100.00,
			img:require('../images/salad.jpg')
		},{
			id:'asdsadce',
			name:"Sesame sausages",
			shortDisc:"Sizzling sausages made from sesame ",
			calories:78,
			price:100.00,
			img:require('../images/sausage.jpg')
		},{
			id:'cdsdds',
			name:"Tomato and spinach pasta",
			shortDisc:"Egg-free pasta made from tomatoes",
			calories:78,
			price:100.00,
			img:require('../images/pasta.jpg')
		}];  
		//   console.log(this.state.itemList);

        return (
            <SafeAreaView style={styles.body}>
                <View style={{flexDirection:"row",alignItems:"baseline", justifyContent:"center"}}>
                    <View style={styles.searchComponent}>
                        <Image  style={styles.icon} source={require('../images/search.png')} />
                        <TextInput placeholder="Search Food" value={this.state.queryText} style={styles.searchBar} onChangeText={(e)=>this.onChangeSearchHandler(e)} />
                    </View>
                    <TouchableOpacity onPress={()=>this.getFoodItems(this.state.queryText)} style={styles.enterKeyContainer}>
                        <Image style={styles.enterKey}  source={require("../images/right-arrow.png")} />
                    </TouchableOpacity>
                </View>
                <Text style={{marginTop:40,marginBottom:30,fontSize:22,textAlign:"center", fontWeight:"700"}}>Found {this.state.itemList.length} results </Text>
                <ScrollView contentContainerStyle={{paddingBottom:30, flexDirection:"row",flexWrap:"wrap",alignItems:"center",justifyContent:"center"}} showsVerticalScrollIndicator={false} horizontal={false} >
                    <FoodCard navigation={this.props.navigation} isSearchQry={true} fooditems={this.state.itemList} />
                </ScrollView>
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    body:{
		paddingTop: Platform.OS === 'android' ? 30 : 0,
		paddingLeft:15,
		paddingRight:10,
		paddingBottom:10,
		backgroundColor:'#fff',
		height:'100%'
	},	
    icon:{
		width:20,
		height:20
	},
	searchComponent:{
		backgroundColor:'#fbfbfb',
		marginTop:30,
		padding:10,
		flexDirection:"row",
		alignItems:"center",
		width:'70%',
	},	
	searchBar:{
		fontSize:16,
		paddingLeft:15,
		paddingRight:15
	},
	enterKeyContainer:{
		borderRadius:3,
		borderWidth:0.6,
		borderColor:"#f3f3f3",
		padding:10,
		marginLeft:10
	},	
	enterKey:{
		paddingLeft:5,
		width:25,
		height:25
	},
})
export default SearchQry;