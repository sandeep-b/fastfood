import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import CookieManager from '@react-native-community/cookies';

export default class Login extends Component {

	constructor(props) {
		super(props);
		state = {
			email   : '',
			password: '',
			isLogin:false,
			accessToken:''
		}
	}

	onClickListener = (viewId) => {
		Alert.alert("Alert", "Button pressed "+viewId);
	}
	onClickLoginListener = () => {
		fetch('http://d2de7ff5e729.ngrok.io/auth/login', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: this.state.email,
				password: this.state.password
			})
		}).then(res => res.json()).then(data=>{
			console.log(data);
			if ( data.status === 'Success' ){
				this.setState({
					accessToken:data.accessToken,
					isLogin:true
				})
				this.storeData(data.email, data.accessToken)
				// this.props.navigation.navigate('Home')
			}
		})
	}

	storeData = async (email, accessToken) => {

		// CookieManager.set('', {
		// 	name: 'myCookie',
		// 	value: 'myValue',
		// 	domain: 'some domain',
		// 	path: '/',
		// 	version: '1',
		// 	expires: '2015-05-30T12:30:00.00-05:00'
		// }).then((done) => {
		// 	console.log('CookieManager.set =>', done);
		// });
		try {
		  const jsonValue = JSON.stringify(accessToken)
		  await AsyncStorage.setItem('@presto-apps', jsonValue)
		} catch (e) {
		  // saving error
		  console.log(e);
		}
	}

	fetchUserData = async () => {
		try {
			const value = await AsyncStorage.getItem('@presto-apps')
			console.log(value);
			if(value !== null) {
			// value previously stored
			}
		} catch(e) {
			// error reading value
			console.log("fetch error");
			console.log(e);
		}
	}
	  
	render() {
		console.log(this.state);
		
		return (
			<View style={styles.container}>
				<View style={styles.inputContainer}>
				<Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
				<TextInput style={styles.inputs}
					placeholder="Email"
					keyboardType="email-address"
					underlineColorAndroid='transparent'
					onChangeText={(email) => this.setState({email})}/>
				</View>
				
				<View style={styles.inputContainer}>
				<Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/>
				<TextInput style={styles.inputs}
					placeholder="Password"
					secureTextEntry={true}
					underlineColorAndroid='transparent'
					onChangeText={(password) => this.setState({password})}/>
				</View>

				<TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.onClickLoginListener()}>
				<Text style={styles.loginText}>Login</Text>
				</TouchableHighlight>

				<TouchableHighlight style={styles.buttonContainer} onPress={() => this.onClickListener('restore_password')}>
					<Text>Forgot your password?</Text>
				</TouchableHighlight>

				<TouchableHighlight style={styles.buttonContainer} onPress={() => this.fetchUserData()}>
					<Text>Register</Text>
				</TouchableHighlight>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
	flex: 1,
	justifyContent: 'center',
	alignItems: 'center',
	backgroundColor: '#DCDCDC',
  },
  inputContainer: {
	  borderBottomColor: '#F5FCFF',
	  backgroundColor: '#FFFFFF',
	  borderRadius:30,
	  borderBottomWidth: 1,
	  width:250,
	  height:45,
	  marginBottom:20,
	  flexDirection: 'row',
	  alignItems:'center'
  },
  inputs:{
	  height:45,
	  marginLeft:16,
	  borderBottomColor: '#FFFFFF',
	  flex:1,
  },
  inputIcon:{
	width:30,
	height:30,
	marginLeft:15,
	justifyContent: 'center'
  },
  buttonContainer: {
	height:45,
	flexDirection: 'row',
	justifyContent: 'center',
	alignItems: 'center',
	marginBottom:20,
	width:250,
	borderRadius:30,
  },
  loginButton: {
	backgroundColor: "#00b5ec",
  },
  loginText: {
	color: 'white',
  }
});
