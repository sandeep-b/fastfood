import React from 'react'
import {View, Image,Text,Button, StyleSheet} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationContext } from '@react-navigation/native';


class Header extends React.Component {
    static contextType = NavigationContext;

    signoutFun = async(navigation) => {
        const signout = await AsyncStorage.clear();
        navigation.navigate('Login')
     }
    render(){
        const navigation = this.context;
        return (
            <View style={styles.header}>
                <View style={{flexDirection:'row', alignItems:"center" }}>
                    <Image style={styles.logo}  source={require('../images/logo.png')} />
                    <Text style={{fontSize:20, paddingLeft:10, color:'#F79411', fontWeight:'600'}}>Fast Food</Text>
                </View>
                <Button onPress={()=>this.signoutFun(navigation)} title="signout">
                    <Image style={styles.user}  source={require('../images/user.png')} />
                </Button>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header:{
        padding:15,
        paddingRight:20,
        justifyContent:"space-between",
        flexDirection:"row",
        position:"absolute",
        top:30,
        left:0,
        right:0,
        backgroundColor:'#fefefe',
        zIndex:1000
    },
    logo:{
        height:40,
        width:40
    },  
    user:{
        width:35,
        height:35,

    }    
})
export default Header

