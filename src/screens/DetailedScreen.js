import React, { Component } from 'react';
import {View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    TextInput,
    ScrollView,
    SafeAreaView, Button
} from 'react-native'
import ImageView from 'react-native-image-view';


class DetailedScreen extends Component {
    state = { 
        itemCount:1,
        isImageViewVisible:false,
        showIndex:0
    }
    
    incrementHandler =()=>{
        this.setState({
            itemCount:this.state.itemCount+1
        })
    }
    decrementHandler = () =>{
        this.setState({
            itemCount:this.state.itemCount != 0 ? this.state.itemCount-1 : 0
        })
    }

    viewImg = (value) => {
        console.log(value);
        this.setState({
            showIndex:value,
            isImageViewVisible:true
        })
    }
    
    onCloseGalaryHandler = () => {
        this.setState({
            isImageViewVisible:false
        })
    }

    render() {
        const images = [
            {
                source: {
                    uri: 'https://cdn.pixabay.com/photo/2015/09/02/12/43/meal-918639_960_720.jpg',
                },
                title: 'Food1',
                width: 806,
                height: 720,
            },
            {
                source: {
                    uri: 'https://cdn.pixabay.com/photo/2014/11/05/15/57/salmon-518032_960_720.jpg',
                },
                title: 'Food2',
                width: 806,
                height: 720,
            },
            {
                source: {
                    uri: 'https://cdn.pixabay.com/photo/2014/10/19/20/59/hamburger-494706__340.jpg',
                },
                title: 'Food3',
                width: 806,
                height: 720,
            },
        ];

        const ImgGalary = ({imgList}) => {            
            var resp =  imgList.map( (img,i) => {
                return(
                    <TouchableOpacity key={i}  onPress={()=>this.viewImg(i)} >
                        <Image source={{uri:img.source.uri}} style={{width:200,height:200, marginRight:10}} />
                    </TouchableOpacity>
                )
            })
            return resp;
        }
        
        return (
            <SafeAreaView style={styles.body}>
                <View style={styles.headding}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Image resizeMode="contain" style={styles.headingIcons} source={require('../images/back.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity >
                        <Image resizeMode="contain" style={[styles.headingIcons,{width:35,height:25}]} source={require('../images/cart.png')} />
                    </TouchableOpacity>
                </View>
                <ScrollView contentContainerStyle={styles.sectionBody} showsVerticalScrollIndicator={false}>
                    <Image resizeMode="contain" style={styles.foodImg} source={require('../images/food1.jpg')} />
                    <View style={styles.counterBtnArea}>
                        <TouchableOpacity onPress={()=>this.decrementHandler()} style={styles.counterBtn} >
                            <Image style={{width:12, marginRight:8}} resizeMode="contain" source={require('../images/minus.png')} />
                        </TouchableOpacity>
                        <Text style={{fontSize:30,fontWeight:"700", marginRight:8}}>{this.state.itemCount}</Text>
                        <TouchableOpacity onPress={()=>this.incrementHandler()} style={styles.counterBtn} >
                            <Image style={{width:14}} resizeMode="contain" source={require('../images/plus.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.foodTitlearea}>
                        <Text style={{fontSize:22,fontWeight:"600", maxWidth:"70%", flexWrap:"wrap"}}>{this.props.route.params.name}</Text>
                        <Text style={{fontSize:30}}><Text style={{fontSize:18}}>Rs.</Text>{this.props.route.params.price}</Text>
                    </View>
                    <View style={[styles.foodTitlearea,{justifyContent:"space-around"}]}>
                        <View style={styles.foodInfo}>
                            <Image style={{width:20,marginRight:6}} resizeMode="contain" source={require('../images/star.png')} />
                            <Text style={{fontSize:16,  flexWrap:"wrap"}}>2.6</Text>
                        </View>
                        <View style={styles.foodInfo}>
                            <Image style={{width:20,marginRight:6}} resizeMode="contain" source={require('../images/calories.png')} />
                            <Text style={{fontSize:16,  flexWrap:"wrap"}}>80 calories</Text>
                        </View>
                        <View style={styles.foodInfo}>
                            <Image style={{width:20,marginRight:6}} resizeMode="contain" source={require('../images/clock.png')} />
                            <Text style={{fontSize:16,  flexWrap:"wrap"}}>10 - 30 min</Text>
                        </View>
                    </View>
                    <View style={{width:"90%",paddingHorizontal:15,marginTop:20}}>
                        <Text style={{fontSize:18, fontWeight:"700"}}>Details</Text>
                        <Text style={{marginTop:10,fontSize:16, color:"#666666"}}>{this.props.route.params.disc}</Text>
                    </View>
                    <View style={{width:"90%",paddingHorizontal:15,marginTop:20, marginBottom:50}}>
                        <Text style={{fontSize:18, fontWeight:"700", marginBottom:10}}>Reviews</Text>
                        <ImageView
                            images={images}
                            imageIndex={this.state.showIndex}
                            animationType="slide"
                            isVisible={this.state.isImageViewVisible}
                            onClose={()=>this.onCloseGalaryHandler()}
                        />
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <ImgGalary imgList={images}  />
                        </ScrollView>
                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    body:{
		paddingTop: Platform.OS === 'android' ? 30 : 0,
		paddingLeft:5,
		paddingRight:5,
		paddingBottom:10,
		backgroundColor:'#fff',
		height:'100%'
    },	
    foodInfo:{
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"center", 
        marginRight:15
    },
    foodTitlearea:{
        width:"90%",
        paddingHorizontal:15,
        marginTop:20,
        flexDirection:"row",
        alignItems:"flex-start",
        justifyContent:"space-between",
    },
    counterBtnArea:{
        backgroundColor:"#ffc271",
        flexDirection:"row",
        alignItems:"center",
        paddingHorizontal:20,
        paddingVertical:3,
        borderRadius:25,
        
    },  
    headding:{
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-between",
        padding:10,
        marginTop:20
    },
    headingIcons:{
        width:30,
        height:20
    },
    sectionBody:{
        marginTop:0,
        alignItems:"center"
    },
    foodImg:{
        width:"70%",
        height:250,
    }
})
export default DetailedScreen;