import React from 'react';

import {
    View,
    Image,
    SafeAreaView,
    StyleSheet, 
    Button,
    Text,
	Platform,
	ScrollView,
	TouchableOpacity,
	TextInput,
} from 'react-native'
import Header from './header'
import FoodCard from "../components/foodCard"

import PushNotification from '../components/pushNotification'
class Home extends React.Component {
    state={
		queryText:'',
		category:'All'
	}
	
	onChangeSearchHandler = (value) => {
		this.setState({
			queryText:value
		})
	}
	onClickCatogryHandler = (value) => {
		this.setState({
			category:value
		})
	}
    render() {  
		let fooditems = [{

			id:'jndsajdj',
			name:"Rocket and tomato spaghetti",
			shortDisc:"Spagetti topped with fresh rocket",
			calories:78,
			price:100.00,
			img:require('../images/spaghetti.jpg')
		},{
			id:'sdsdsd',
			name:"Veg Sandwich",
			shortDisc:"Crunchy bread made with fresh rosemary ",
			calories:78,
			price:100.00,
			img:require('../images/sandwich.jpg')
		},{
			id:'jndsdsdajdj',
			name:"Prosciutto and urid dal salad",
			shortDisc:"Egg-free salad made from tomatoes",
			calories:78,
			price:100.00,
			img:require('../images/salad.jpg')
		},{
			id:'asdsadce',
			name:"Sesame and peppercorn sausages",
			shortDisc:"Sizzling sausages made from sesame ",
			calories:78,
			price:100.00,
			img:require('../images/sausage.jpg')
		},{
			id:'cdsdds',
			name:"Tomato and spinach pasta",
			shortDisc:"Egg-free pasta made from tomatoes",
			calories:78,
			price:100.00,
			img:require('../images/pasta.jpg')
		}];  
		
        return (
            <SafeAreaView style={styles.body}>
				<Header  />

				<ScrollView style={styles.homeBodyContent} alwaysBounceVertical={true} showsVerticalScrollIndicator={false} >
					<Text style={styles.mainHeading}>Food that {"\n"}Meets your need</Text>
					<View style={{flexDirection:"row",alignItems:"baseline"}}>
						<View style={styles.searchComponent}>
							<Image  style={styles.icon} source={require('../images/search.png')} />
							<TextInput placeholder="Search food" value={this.state.queryText} style={styles.searchBar} onChangeText={(e)=>this.onChangeSearchHandler(e)} />
						</View>
						<TouchableOpacity onPress={()=>this.props.navigation.navigate("Search",{qry:this.state.queryText})} style={styles.enterKeyContainer}>
							<Image style={styles.enterKey}  source={require("../images/right-arrow.png")} />
						</TouchableOpacity>
					</View>
					<ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={styles.foodCategorySection}>
						<TouchableOpacity style={[styles.catogaryCard, this.state.category == 'All' ? styles.categoryActive : {} ]} onPress={()=>this.onClickCatogryHandler("All")}>
							<Image resizeMode="contain" style={styles.icon} source={require("../images/4.png")} />
							<Text style={{paddingLeft:10}}>All</Text>
						</TouchableOpacity>
						<TouchableOpacity style={[styles.catogaryCard, this.state.category == 'Fast Food' ? styles.categoryActive : {} ]} onPress={()=>this.onClickCatogryHandler("Fast Food")}>
							<Image resizeMode="contain" style={styles.icon} source={require("../images/4.png")} />
							<Text style={{paddingLeft:10}}>Fast Food</Text>
						</TouchableOpacity>
						<TouchableOpacity style={[styles.catogaryCard, this.state.category == 'Main' ? styles.categoryActive : {} ]} onPress={()=>this.onClickCatogryHandler("Main")}>
							<Image resizeMode="contain"  style={styles.icon} source={require("../images/4.png")} />
							<Text style={{paddingLeft:10}}>Main</Text>
						</TouchableOpacity>
						<TouchableOpacity style={[styles.catogaryCard, this.state.category == 'Drinks' ? styles.categoryActive : {} ]} onPress={()=>this.onClickCatogryHandler("Drinks")}>
							<Image resizeMode="contain" style={styles.icon} source={require("../images/4.png")} />
							<Text style={{paddingLeft:10}}>Drinks</Text>
						</TouchableOpacity>
						<TouchableOpacity style={[styles.catogaryCard, this.state.category == 'Dessert' ? styles.categoryActive : {} ]} onPress={()=>this.onClickCatogryHandler("Dessert")}>
							<Image resizeMode="contain" style={styles.icon} source={require("../images/4.png")} />
							<Text style={{paddingLeft:10}}>Dessert</Text>
						</TouchableOpacity>
					</ScrollView>
					<PushNotification />
					<ScrollView contentContainerStyle={{marginTop:40,paddingBottom:30,flexDirection:"row",}} horizontal={true} showsHorizontalScrollIndicator={false} >
						<FoodCard navigation={this.props.navigation} fooditems={fooditems} />
					</ScrollView>
					<View style={{height:100}}>
					</View>
					
				</ScrollView>
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    body:{
		paddingTop: Platform.OS === 'android' ? 30 : 0,
		paddingLeft:30,
		paddingRight:10,
		paddingBottom:10,
		backgroundColor:'#fff',
		height:'100%'
	},
	img:{
		width:'100%',
		maxHeight:300
	},
	homeBodyContent:{
		paddingTop:100,
		paddingBottom:20
	},
	mainHeading:{
		fontSize:35,
		lineHeight:50
	},
	icon:{
		width:20,
		height:20
	},
	searchComponent:{
		backgroundColor:'#fbfbfb',
		marginTop:30,
		padding:10,
		flexDirection:"row",
		alignItems:"center",
		width:'70%',
	},	
	searchBar:{
		fontSize:16,
		paddingLeft:15,
		paddingRight:15
	},
	enterKeyContainer:{
		borderRadius:3,
		borderWidth:0.6,
		borderColor:"#f3f3f3",
		padding:10,
		marginLeft:10
	},	
	enterKey:{
		paddingLeft:5,
		width:25,
		height:25
	},
	catogaryCard:{
		padding:10,
		flexDirection:"row",
		borderWidth:1,
		borderColor:"#f2f2f2",
		borderRadius:10,
		marginRight:15
	},
	categoryActive:{
		backgroundColor:"#ffc271",
	},
	foodCategorySection:{
		flexDirection:"row",
		flexWrap:"nowrap",
		marginTop:30,
	},
	

});

export default Home;