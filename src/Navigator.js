import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import Home from './screens/Home'
import LoginView from './screens/Login'
import Detail from './screens/DetailedScreen'
import SearchQry from './screens/SearchQry'
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AppState} from 'react-native';

const Stack = createStackNavigator();
const screenOptionStyle = {
    headerShown:false
}

class HomeStackNavigator extends React.Component {
    state = {
        isTokenExist:false,
        appState: AppState.currentState
    }
    
    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
        this.fetchUserData()
    }
 
    componentWillUnmount() {
        // AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleAppStateChange = (nextAppState) => {
        // console.log("sandhshf----"+nextAppState);
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            console.log('App has come to the foreground!')
        }
        this.setState({appState: nextAppState});
    }
   
    fetchUserData = async () => {
        try {
            const value = await AsyncStorage.getItem('@presto-apps')
            // console.log(value);
            if ( value !== null ) {
                this.setState({
                    isTokenExist:true
                })
            }
        } catch(e) {
            // error reading value
            console.log("fetch error");
            console.log(e);
        }
    }
    render(){
        console.log(this.state);
        return(
            <Stack.Navigator screenOptions={screenOptionStyle}>
                { this.state.isTokenExist ? 
                    <React.Fragment>
                        <Stack.Screen name="Home" component={Home}/>
                        <Stack.Screen name="Search" component={SearchQry}/>
                        <Stack.Screen name="Detail" component={Detail}/>
                        {/* <Stack.Screen name="Login" component={LoginView}/>  */}
                    </React.Fragment>
                        :
                    <Stack.Screen name="Login" component={LoginView}/> 
                }
                
            </Stack.Navigator>
        )
    }
}
export default HomeStackNavigator;