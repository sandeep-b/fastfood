import React, { Component, useState} from 'react';
import {
    Image,
    Text,
    TouchableOpacity,
    View,
    StyleSheet
} from 'react-native'


class foodCard extends Component {
    state={
        isSearchQry:this.props.isSearchQry != undefined ? this.props.isSearchQry : false
    }
    render() {
        
        let card = this.props.fooditems.map(item => {
            return(
                <TouchableOpacity 
                    onPress={()=>this.props.navigation.navigate("Detail",{name:item.name,price:item.price,disc:item.shortDisc})}
                    key={item.id} 
                    style={this.props.isSearchQry == undefined ? styles.detaildItem : styles.detaildItemInSearch}>
                        <Image resizeMode="contain" style={this.props.isSearchQry == undefined ? styles.detaildItemImg : styles.detaildItemImgSearch} source={require('../images/5.png')} />
                        <Text style={{textAlign:"center", fontSize:18, fontWeight:"700", marginBottom:10}}>{item.name}</Text>
                        <Text style={{textAlign:"center", fontSize:14, color:'#9e9e9e',marginBottom:10}}>{item.shortDisc}</Text>
                        
                        <Text style={{textAlign:"center", fontSize:28, fontWeight:"700"}}>
                            <Text style={{ fontSize:16,color:'#ffc271', fontWeight:"500"}}>Rs.</Text>{item.price}
                        </Text>
                </TouchableOpacity>
            )
        })
        return card;
    }
} 
const styles = StyleSheet.create({
    detaildItem:{
		width:250,
		borderWidth:1,
		borderRadius:10,
		borderColor:'#f2f2f2',
		marginRight:5,
		padding:10,
		alignItems:"center"
    },
	detaildItemImg:{
		width:"90%",
		height:200,
		resizeMode:"cover",
    },
    detaildItemImgSearch:{
        width:"90%",
		height:150,
		resizeMode:"contain",
    },
    detaildItemInSearch:{
        width:"46%",
		borderWidth:1,
		borderRadius:10,
		borderColor:'#f2f2f2',
		marginRight:5,
        padding:10,
        minHeight:300,
        alignItems:"center",
        justifyContent:"center",
        marginBottom:10
    }
    
})
export default foodCard;


